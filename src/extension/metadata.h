//
//  metadata.h
//
//  Created by David Tourigny on 10/16/19.
//

#ifndef metadata_h
#define metadata_h

#include <stdlib.h>
#include <string>

struct MetaData
{
    MetaData(std::string text) { genome_name = text; };
    std::string genome_name; //
    std::string genome_dir; //
    std::string global_net_file; //
    
    double coeff_hm; // coeff for homology (sequence identity score) (+)
    double coeff_ph; // coeff for phylogenetic corr (+)
    double coeff_gc; // coeff for gene clustering (+)
    double coeff_ce; // coeff for co-expression (+)
    double coeff_ort; //coeff for othology (+)
    double p_ec_co_occur; // coeff for EC-EC co-occur (+)
    double p_no_EC; // coeff for not assigning EC to a gene (penalty) (-)
    double median_ec_co_occur; // coeff of median ec-ec occurence (coff_median_ec_occur = median of all BN ec-ec occurence.
    
    int cgibbs(int, std::string);
};

#endif

