
# coding: utf-8

import sys #only needed to determine Python version number
import pandas as pd

def func(x, query_hit_dict, query_BS_dict, fout):
    best_gen_name = x['sseqid']
    query = x['qseqid']
    # if Best hits match in both directions
    if best_gen_name in query_hit_dict and query_hit_dict[best_gen_name] == query:
        fout.write(best_gen_name + '\t' + query + '\t' + str(query_BS_dict[best_gen_name]) + '\n')

def get_bbhit(genome_name, swissprot, database):
    modelID_1 = genome_name # .fas
    speID = swissprot # spe
    # modelID_1 = '1035191.3.fas'
    # speID = 'sce' # no organism called atc

    Dir1 = './results/' + modelID_1 + '/blast/'

    Dir2 = database

    blastp_fname_1 = Dir1 + speID + '.vs.' + modelID_1 + '.blastp'
    blastp_fname_2 = Dir1 + modelID_1 + '.vs.' + speID + '.blastp'

    out_filename = Dir1 + modelID_1 + '.vs.' + speID + '.bbhs'

    if False:
        '''
        04-Get_BBHs.pl
        /results/$b/blast/${b}.vs.${a}.blastp # b=genome, a=organism
        /results/$b/blast/${a}.vs.${b}.blastp
        > /results/$b/blast/${b}.vs.${a}.bbhs

        05-Get_Orthology.pl
        /results/$1/$1.blast.bbh.out
        /OLD_CONTEXT/KEGG2Uniprot.txt
        /OLD_CONTEXT/Swissprot2Uniprot.txt
        /OLD_CONTEXT/Swissprot2EC.txt
        /database/GRGENEECnet.txt
        /results/$1/$1.gene
        $1

        > /results/$1/$1.ort
        '''

    #default_outfmt6_cols =  'qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore'.strip().split(' ')
    #default_outfmt6_cols =  'qseqid sseqid qlen slen pident length mismatch gaps qstart qend sstart send evalue bitscore'.strip().split(' ')
    default_outfmt6_cols =  'qseqid sseqid qlen slen pident mismatch gaps qstart qend sstart send evalue bitscore'.strip().split(' ')

    # df is a pandas.DataFrame and blastp_fname won't have a header
    df_gen2org = pd.read_table(blastp_fname_2, header=None, names=default_outfmt6_cols)
    df_org2gen = pd.read_table(blastp_fname_1, header=None, names=default_outfmt6_cols)

    # Kepp the best hits with the highest bitscores only
    gp_df = df_gen2org['bitscore'].groupby(df_gen2org['qseqid'])
    max_index_df_gen2org = gp_df.idxmax().tolist()
    df_gen2org = df_gen2org.filter(max_index_df_gen2org, axis='index')

    #plt.hist(df_gen2org['send'] - df_gen2org['sstart'])
    #plt.hist(df_gen2org['qend'] - df_gen2org['qstart'])

    ### TODO ###
    # Strict threshold (combination of the length of the alignment, e-value, and bitscore )
    length_cr = (df_gen2org['send'] - df_gen2org['sstart'] + 1 >= 40) & (df_gen2org['qend'] - df_gen2org['qstart'] + 1 >= 40)
    frac_cr = (df_gen2org['send'] - df_gen2org['sstart'] + 1>= 0.7*df_gen2org['slen'] ) & (df_gen2org['qend'] - df_gen2org['qstart'] + 1>= 0.7*df_gen2org['qlen'] )
    bitsc_cr = df_gen2org['bitscore'] > 26.4 # from bitscore distribution.
    eval_cr = df_gen2org['evalue'] < 2 # from evalue distribution.

    df_gen2org_filtered = df_gen2org[(length_cr | frac_cr) & bitsc_cr & eval_cr]
    df_gen2org_filtered = df_gen2org[df_gen2org['bitscore'] > 26.4]
    
    df_gen2org_filtered = df_gen2org_filtered.set_index('qseqid')
    query_hit_dict = df_gen2org_filtered['sseqid'].to_dict()
    query_BS_dict = df_gen2org_filtered['bitscore'].to_dict()

    #Finding BBH
    fout = open(out_filename,'w')
    gp_df = df_org2gen['bitscore'].groupby(df_org2gen['qseqid']) # org to genome
    max_index_df_org2gen = gp_df.idxmax().tolist()
    df_org2gen_test = df_org2gen.filter(max_index_df_org2gen, axis='index')
    df_org2gen_test.apply(lambda row: func(row,query_hit_dict,query_BS_dict,fout), axis=1)
    fout.close()
    fout = open(out_filename,'r')
    return fout
        
def get_all(genome_name, database):

    Dir1 = database
    Dir2 = './results/' + genome_name + '/'
    swiss_cluster_filename = Dir1 + 'SwissCluster_Genomes.txt'
    outputfile_name = Dir2 + genome_name + '.blast.bbh.out'

    outputfile = open(outputfile_name, 'w')
    with open(swiss_cluster_filename) as swissprot_file:
        lines = list(swissprot_file)
        first_line = lines[0] #There should only be one line in file
        split_line = first_line.split()
        for i in range(len(split_line)):
            ref_swissprot = split_line[i]
            results_file = get_bbhit(genome_name,ref_swissprot,database)
            outputfile.write(results_file.read())
