
# coding: utf-8

import sys #only needed to determine Python version number
#from Bio import SeqIO, Seq, SeqRecord
import pandas as pd

def parse_blastp(genome1_name, genome2_name, database):
    modelID1 = genome1_name #'1035191.3.fas'
    modelID2 = genome2_name #'a.aeolicus.pep'

    Dir1 = './results/' + modelID1 + '/profile/'
    Dir2 = database

    eval_cufoff = 0.001

    #if len(sys.argv)>3:
    #    eval_cufoff = sys.argv[4] #sys.argv[0] = xxx.py

    # blastp_fname are from blast (C-BlastProfileGenomes.sh)
    blastp_fname = Dir1 + modelID1 + '.TO.' + modelID2 + '.blastp'

    out_fname = Dir1 + modelID1 + '.TO.' + modelID2 + '.parsed'

    outfmt6_cols = 'qseqid sseqid evalue'.strip().split(' ')

    df = pd.read_table(blastp_fname, header=None, names=outfmt6_cols)

    fout = open(out_fname,'w')
    gp_df = df.groupby('qseqid')
    keys = gp_df.groups.keys() # qseqid
    keys = sorted(keys)
    for query in keys: # each query
        q_gp_df = gp_df.get_group(query)
        
        # sort by e-val in descending order
        q_df = q_gp_df.sort_values(by='evalue', ascending=True, inplace=False)
        if q_df.iloc[0]['evalue']<=eval_cufoff:
            fout.write( query + '\t1\n')
        else:
            fout.write( query + '\t0\n')
    
    fout.close()

def parse_all(genome_name, database):

    Dir1 = database
    genomes_filename = Dir1 + '71_Genomes.txt'

    with open(genomes_filename) as genomes_file:
        lines = list(genomes_file)
        first_line = lines[0] #There should only be one line in file
        split_line = first_line.split()
        for i in range(len(split_line)):
            ref_genome = split_line[i]
            parse_blastp(genome_name,ref_genome,database)
