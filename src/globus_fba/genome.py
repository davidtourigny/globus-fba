import sys, os
from pathlib import Path

from globus_fba import ublast
from globus_fba import homology
from globus_fba import bprofile
from globus_fba import phylogeny
from globus_fba import context
from globus_fba import bbhits
from globus_fba import clustering
from globus_fba import orthology
from globus_fba import cgibbs

class Genome():
    """Genome is a class for running the GLOBUS pipeline on a given genome (an instance of this class).
        
        Parameters
        ----------
        id: string
            The identifier to associate with this exchange flux.
        
        lower_bound_expression: optlang.symbolics expression
            The symbolic expression for calculating lower bound of this exchange flux.
        
        upper_bound_expression: optlang.symbolics expression
            The symbolic expression for calculating upper bound of this exchange flux.
        """
    
    def __init__(self, name):
        self._id = name
        self._metadata = cgibbs.MetaData(name)
        self.metadata.genome_dir = os.getcwd() + "/results/"

    def run_homology(self):
        """This function links the homology to be used for GLOBUS."""
        
        homology.link_homology(self._id, self.database)
    
    def run_blastprofile(self):
        """This function parses all BLAST profiles to be used for GLOBUS."""
        bprofile.parse_all(self._id, self.database)
    
    def run_phylogeny(self):
        """This function aligns and correlates the phylogeny to be used for GLOBUS."""
        
        phylogeny.align_phylogeny(self._id)
        phylogeny.correlate_phylogeny(self._id)

    def run_ublast(self):
        """This function parses the ublast to be used for GLOBUS."""
        
        ublast.parse_ublast(self._id, self.database)
    
    def run_context(self):
        """This function computes the zscores from context to be used for GLOBUS."""
        
        context.calculate_zscores(self._id)
    
    def run_bbhits(self):
        """This function computes the best bidirectional hits to be used for GLOBUS."""
        
        bbhits.get_all(self._id, self.database)
            
    def run_orthology(self):
        """This function computes the orthology to be used for GLOBUS."""
        
        orthology.get_orthology(self._id, self.database)
    
    def run_clustering(self):
        """This function computes distances on the chromosome between orthologs of each pair of genes"""
        
        clustering.keep_gene_bbhs(self._id)
        clustering.make_dummy_order(self._id)
        clustering.get_clustering(self._id, self.database)
        clustering.cal_z_score(self._id)
    
    def run_cgibbs(self):
        """This function calls the C++ function to perform Gibbs sampling after initializing the remaining metadata.
            """
        self.metadata.coeff_hm = 0.958108
        self.metadata.coeff_ph = 0.288343
        self.metadata.coeff_gc = 0.782456
        self.metadata.coeff_ce = 0.0
        self.metadata.coeff_ort = 0.808274
        self.metadata.p_ec_co_occur = 0.886837
        self.metadata.p_no_EC = -1.75286
        self.metadata.median_ec_co_occur = 0.1179
        self.metadata.cgibbs(1,"2")


    @property
    def metadata(self):
        return self._metadata
    
    @property
    def database(self):
        return self._database
    
    @database.setter
    def database(self, databasePath):
        """This function sets the database to be used for GLOBUS.
            If required files are not contained within then an exception is raised.
            
            Parameters
            ----------
            databasePath : string
            path of directory containing GLOBUS database.
            Should be of format XXX/database_name/ (e.g., with "/" at end).
            """
        
        #check database for 71_Genomes.txt
        genomes_file = Path(databasePath + "71_Genomes.txt")
        if not genomes_file.is_file():
            raise Exception("File 71_Genomes.txt does not exist in database!")
        
        #check database for GRGENEECnet_2014.txt
        global_net_file = Path(databasePath + "GRGENEECnet_2014.txt")
        if global_net_file.is_file():
            self.metadata.global_net_file = databasePath + "GRGENEECnet_2014.txt"
        else:
            raise Exception("File GRGENEECnet_2014.txt does not exist in database!")
        
        #check database for UniProt_SwProt_dict.npy
        swiss_prot_dict = Path(databasePath + "UniProt_SwProt_dict.npy")
        if not swiss_prot_dict.is_file():
            raise Exception("File UniProt_SwProt_dict.npy does not exist in database!")
        
        #check database for SwProt_kegg_dict.npy
        swiss_prot_kegg_dict = Path(databasePath + "SwProt_kegg_dict.npy")
        if not swiss_prot_kegg_dict.is_file():
            raise Exception("File SwProt_kegg_dict.npy does not exist in database!")
        
        #check database for SwProt_EC_dict.npy
        swiss_prot_ec_dict = Path(databasePath + "SwProt_EC_dict.npy")
        if not swiss_prot_ec_dict.is_file():
            raise Exception("File SwProt_EC_dict.npy does not exist in database!")
        
        #check database for SwProt_EC_dict.npy
        spe_multi = Path(databasePath + "SPEmulti.merge2")
        if not spe_multi.is_file():
            raise Exception("File SPEmulti.merge2 does not exist in database!")
        
        #check database for SwissCluster_Genomes.txt
        swiss_cluster = Path(databasePath + "SwissCluster_Genomes.txt")
        if not swiss_cluster.is_file():
            raise Exception("File SwissCluster_Genomes.txt does not exist in database!")
        
        #check database for genomes.txt
        genomes_txt = Path(databasePath + "genomes.txt")
        if not genomes_txt.is_file():
            raise Exception("File genomes.txt does not exist in database!")
        
        self._database = databasePath



