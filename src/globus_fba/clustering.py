import sys, os
import numpy as np
import pandas as pd

#really, genome_name used for ref_genomes should be different from genome_name used for genes_ord, bbh_file, and out_fname
def get_clustering(genome_name, database):
  genomes = database + 'genomes.txt'
  genes_ord = './results/' + genome_name + '/' + genome_name + '.dummy_order'
  genomes_dir = database + 'Gene_order113/'
  bbh_file = './results/' + genome_name + '/' + genome_name + '.blast.bbh.filtered.out'
  out_fname = './results/' + genome_name + '/' + genome_name + '.gc'
  #load the data tables:
  genes_ord_df = pd.read_csv(genes_ord, sep='\t', header=None)
  genomes_df = pd.read_csv(genomes, header=None)
  #remove the query genome from reference genomes:
  genomes_df = genomes_df.drop(np.where(genomes_df.iloc[:,0]==genome_name)[0].tolist(), axis=0)
  bbhs = pd.read_csv(bbh_file, sep='\t', header=None)
  #load genome objects:
  ref_genomes = genomes_df.apply(lambda row: Genome1(row[0], genomes_dir+row[0]+'.out'), axis=1)
  #convert expensive dataframes to lists and dicts:
  genes = genes_ord_df[0].tolist()
  genomes = genomes_df[0].tolist()
  bbhs_dict = {}
  for g in bbhs[0].unique():
    bbhs_dict[g] = [bbhs[1][j] for j in bbhs[bbhs[0]==g].index]
  #create output file:
  fout = open(out_fname,'w')
  fout.write("Total number of genomes: "+str(len(ref_genomes))+" + 1\n")# not sure why +1
  
  for g1 in genes: # name of query gene
    h1 = bbhs_dict.get(g1,[])# all BBHs for g1 (idxed by name of genome it's in)
    h1_dict = {}
    for kv in h1:
      k,v = kv.split(":")[0], kv
      h1_dict[k] = v

    for g2 in genes[genes.index(g1)+1:]: # name of second query gene
      h2 = bbhs_dict.get(g2,[])# all BBHs for g2 (idxed by name of genome it's in)
      h2_dict = {}
      for kv in h2:
        k,v = kv.split(":")[0], kv
        h2_dict[k] = v
      
      p = 0
      gens_both = list(set(h1_dict).intersection(set(h2_dict)))#genomes in both
      gens_clust = list(set(gens_both).intersection(set(genomes)))#also w/ clust info

      #for genomes that have bbhs to both query genes:
      for gen_cur in gens_clust:
        o1 = h1_dict[gen_cur]
        o2 = h2_dict[gen_cur]
        gen_obj = next((x for x in ref_genomes if x.name == gen_cur), None)
        if (o1 in gen_obj.gene_order) & (o2 in gen_obj.gene_order):
          p = p - np.log(gen_obj.p[gen_obj.getOrderDist(o1,o2)]/0.5) / np.log(10)

      if p != 0:
        fout.write(g1+"\t"+g2+"\t"+'%.8f'%p+"\n")
      else:
        fout.write(g1+"\t"+g2+"\t"+'%.1f'%p+"\n")
  fout.close()

class Genome1:
  def __init__(self, spe, f):
    gene_df = pd.read_csv(f, sep='\t', header=None, index_col=0)
    #chromosome names and lengths:
    chrom_count = gene_df.iloc[:,0].value_counts().tolist()
    nGenes = gene_df.shape[0]
    if len(chrom_count) > 1:#linear
      p = [0] * (max(chrom_count)+2)
      for i in range(1,len(p)):
        for j in range(len(chrom_count)):
          if i <= chrom_count[j]:
            p[i] += (i*(chrom_count[j]-i)+i*(i-1)/2) / (nGenes*(nGenes-1)/2.0)
          else:
            p[i] += chrom_count[j]*1 / nGenes
    else:#circular
      p = [0] * int((max(chrom_count)+1)/2+1)
      for i in range(1,len(p)):
        p[i] = min(2*i/(max(chrom_count)-1), 1)
    self.gene_order = gene_df.loc[~gene_df.index.duplicated(keep='first')].to_dict(orient='index')#there can be duplications
    self.p = p
    self.chrom_count = chrom_count
    self.name = spe
  
  def getOrderDist(self, gen1, gen2):
    if self.gene_order[gen1][1] == self.gene_order[gen2][1]:
      o1 = self.gene_order[gen1][6]
      o2 = self.gene_order[gen2][6]
      if len(self.chrom_count) > 1:#linear
        return abs(o1-o2)
      else:#circular
        if abs(o1-o2) < 0.5*max(self.chrom_count):
          return abs(o1-o2)
        else:
          return max(self.chrom_count) - abs(o1-o2)
    else:
      return max(self.chrom_count)+1

def cal_z_score(genome_name):
    modelID = genome_name # query genome name
    #swissprotv = int(sys.argv[3]) # 1 (old) or 2 (newer)
    #swissprotv = int(sys.argv[2]) # 1 (old) or 2 (newer)

    Dir1 = './results/' + modelID + '/'
    #if swissprotv == 1:
    #    gc_corr_fname = Dir1 + modelID + '.gc_sw1'
    #    SwProt_gene_fname = Dir1 + modelID + '.gene_ec_link_only_sw1'
    #    out_fname = Dir1 + modelID + '.' + 'gc.Z_sw1'
    #else:
    gc_corr_fname = Dir1 + modelID + '.gc'
    SwProt_gene_fname = Dir1 + modelID + '.gene_ec_link_only'
    out_fname = Dir1 + modelID + '.' + 'gc.Z'

    gene_ecIndex_dict = {} # key = a gene index in genome, val = list of ec index in the global network
    gene_in_order = {} # list of unique gene names. Gene whose ec # is in the global network
    # gene_in_order['xxx=1'] = xxx
    #SwProt_gene_fname = Dir1 + modelID_1 + '.gene_ec_link_only'
    with open(SwProt_gene_fname,'r') as f: # 0=index, 1=gene, 2=# of ec annotated to the gene, 3~=ec numbers
        for line in f:
            line = line.rstrip()
            line_entry = line.split()
            gene_in_order[line_entry[1]] = line_entry[1].split('=')[0]
            for i in np.arange(int(line_entry[2])):
                if line_entry[0] in gene_ecIndex_dict:
                    gene_ecIndex_dict[line_entry[0]].append(line_entry[3+i])
                else:
                    gene_ecIndex_dict[line_entry[0]] = [line_entry[3+i]]


    '''
    Total number of genomes: 110 + 1
    YAL001C YAL002W 2.73750514
    YAL001C YAL003W 2.47955202
    YAL001C YAL005C 2.69146405
    YAL001C YAL007C 2.26612897
    '''
    num_genes = 1
    curr_gene = ''
    gene_name_vec = {} # name -> index from 0, ... num_genes-1
    with open(gc_corr_fname,'r') as f:
        line = f.readline()
        for line in f:
            line = line.rstrip() # YAL001C YAL002W 2.73750514
            line_entry = line.split()
            if curr_gene == '':
                curr_gene = line_entry[0]
                #gene_name_vec.append(curr_gene)
                gene_name_vec[curr_gene] = num_genes-1
            else:
                if curr_gene != line_entry[0]:
                    break
            num_genes = num_genes + 1
            #gene_name_vec.append(line_entry[1])
            gene_name_vec[line_entry[1]] = num_genes-1


    Gene_Gene_context_mat = np.zeros([num_genes,num_genes])

    with open(gc_corr_fname,'r') as f:
        line = f.readline()
        
        curr_gene = ''
        row=-1

        for line in f:
            line = line.rstrip() # YAL001C YAL002W 2.73750514
            line_entry = line.split()
            if curr_gene != line_entry[0]:
                if curr_gene != '':
                    # row begins with 0
                    for j in np.arange(row+1,num_genes):
                        Gene_Gene_context_mat[row][j] = vec[j-row-1]
                        Gene_Gene_context_mat[j][row] = vec[j-row-1]
                vec = []
                row += 1
                curr_gene = line_entry[0]
            vec.append(float(line_entry[-1]))
            #vec.append(line_entry[-1])
        for j in np.arange(row+1,num_genes):
            Gene_Gene_context_mat[row][j] = vec[j-row-1]
            Gene_Gene_context_mat[j][row] = vec[j-row-1]


    sub_index_vec = []
    geneName2index = {} # index of sub_index_vec and

    for gene in gene_in_order.keys(): # gene name in .gene file
        name = gene.split('=')[0]
        if name in gene_name_vec.keys():
            if gene_name_vec[name] not in sub_index_vec:
                sub_index_vec.append(gene_name_vec[name])
                geneName2index[name] = len(sub_index_vec)-1
        else:
            print('err')

    sub_index_vec_dup = [] # index of sub_index_vec and
    for gene in gene_in_order.keys(): # gene name in .gene file
        name = gene.split('=')[0]
        sub_index_vec_dup.append(geneName2index[name])


    sub_Gene_Gene_context_mat = Gene_Gene_context_mat[sub_index_vec,:][:,sub_index_vec]


    sub_num_genes = len(sub_index_vec)
    gene_context_mean_vec = np.zeros(sub_num_genes)
    gene_context_std_vec = np.zeros(sub_num_genes)
    for i in np.arange(0,sub_num_genes): # a gene in the network having pc
        x_gene_corr = sub_Gene_Gene_context_mat[i]
        x_gene_corr = x_gene_corr[ ~np.isnan(x_gene_corr) ]
        gene_context_mean_vec[i] = np.mean(x_gene_corr)
        gene_context_std_vec[i] = np.std(x_gene_corr)


    Gene_Gene_Zscore_mat = np.zeros([sub_num_genes,sub_num_genes])

    for i in np.arange(0,sub_num_genes): # a gene in the network having pc
        Gene_Gene_Zscore_mat[i][i] = -3
        
        for j in np.arange(i+1,sub_num_genes):
            if ~np.isnan(sub_Gene_Gene_context_mat[i][j]) and ~np.isnan(gene_context_mean_vec[i]) and ~np.isnan(gene_context_std_vec[i]) and ~np.isnan(gene_context_mean_vec[j]) and ~np.isnan(gene_context_std_vec[j]) and gene_context_std_vec[i] != 0 and gene_context_std_vec[j] != 0:
                # warning is from z1 and z2 calculation
                z1 = (sub_Gene_Gene_context_mat[i][j] - gene_context_mean_vec[i])/gene_context_std_vec[i]
                z2 = (sub_Gene_Gene_context_mat[i][j] - gene_context_mean_vec[j])/gene_context_std_vec[j]
                z_score = np.sqrt(z1*z1 + z2*z2) ##/2.0 GP Error in calculating Z-score
                if np.isnan(z_score):
                    z_score = -2
            else:
                z_score = -2
            Gene_Gene_Zscore_mat[i][j] = z_score
            Gene_Gene_Zscore_mat[j][i] = z_score


    Gene_Gene_Zscore_mat_dup = Gene_Gene_Zscore_mat[sub_index_vec_dup,:][:,sub_index_vec_dup]


    fout = open(out_fname,'w')
    for i in np.arange(0,len(sub_index_vec_dup)): # a gene in the network having pc
        [fout.write(str(Gene_Gene_Zscore_mat_dup[i][j]) + " ") for j in np.arange(0,len(sub_index_vec_dup))]
        fout.write('\n')
    fout.close()
    

def keep_gene_bbhs(genome_name):
    Dir1 = './results/' + genome_name + '/'
    gene_file = Dir1 + genome_name + ".gene"
    bbhs_file = Dir1 + genome_name + ".blast.bbh.out"
    out_fname = Dir1 + genome_name + ".blast.bbh.filtered.out"
    genes = []

    with open(gene_file,'r') as infile:
        lines = list(infile)
        for i in range(len(lines)):
            parts = lines[i].split()
            genes.append(parts[1])

    fout = open(out_fname,'w')
    with open(bbhs_file,'r') as infile:
        lines = list(infile)
        for i in range(len(lines)):
            parts = lines[i].split()
            if parts[0] in genes:
                fout.write(lines[i])
    fout.close()

def make_dummy_order(genome_name):
    Dir1 = './results/' + genome_name + '/'
    genome_file = './' + genome_name
    out_fname = Dir1 + genome_name + ".dummy_order"
    
    fout = open(out_fname,'w')
    with open(genome_file,'r') as infile:
        lines = list(infile)
        for i in range(len(lines)//2):
            line = lines[2*i]
            fout.write(line[1:(len(line)-1)] + "\tchr\t+\t1\t2\t3\t4\n")
    fout.close()
    
