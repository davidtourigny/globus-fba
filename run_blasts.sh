#!/bin/sh
#This shell script should be run before globus pipeline is initiated
#Arguments in format <path to genome file> <path to database>

b=$1 c=$2

if [ "$#" -ne 2 ]
then
    echo "Please supply arguments in format <path to genome file> <path to database>."
    exit 9999
fi

if [ ! -f $b ]
then
    echo "Genome file $b DOES NOT exist."
    exit 9999
fi

if [ ! -d $c ]
then
    echo "Database directory $c DOES NOT exist."
    exit 9999
fi

#Make folders and convert input file to correct format

cut -d ' ' -f 1 $b >> $b.short
awk 'NR==0{c=$0} /^>/{print c;c=$0"\n"} !/^>/{c=c""$0} END{print c}' $b.short > $b.oneline
awk 'NF' $b.oneline > $b.input
rm $b.oneline $b.short
mkdir results
mkdir ./results/$b.input
mkdir ./results/$b.input/profile
mkdir ./results/$b.input/blast

#Blast Swissprot
usearch -ublast $b.input -db $c/sprot_enzyme_reviewed_ec5.fasta.udb -evalue 5e-2 -userout ./results/$b.input/$b.input.VsSprot2_5e-2_out6_seq.blastp -userfields query+target+ql+tl+id+mism+gaps+qlo+qhi+tlo+thi+evalue+bits+qrow+trow

#Blast Profile
for a in $(cat $c/71_Genomes.txt); do usearch -ublast ./$b.input -db $c/71Genomes/$a -evalue 5e-2 -userout ./results/$b.input/profile/$b.input.TO.$a.blastp -userfields query+target+evalue; done

#Blast Ort Forward
for a in $(cat $c/SwissCluster_Genomes.txt); do usearch -ublast ./$b.input -db $c/ClusterGenomes/$a.pep -evalue 5e-2 -maxhits 5 -userout ./results/$b.input/blast/$b.input.vs.$a.blastp -userfields query+target+ql+tl+id+mism+gaps+qlo+qhi+tlo+thi+evalue+bits; done

#Blast Ort Reverse
for a in $(cat $c/SwissCluster_Genomes.txt); do usearch -ublast $c/ClusterGenomes/$a.pep -db ./$b.input -evalue 5e-2 -maxhits 5 -userout ./results/$b.input/blast/$a.vs.$b.input.blastp -userfields query+target+ql+tl+id+mism+gaps+qlo+qhi+tlo+thi+evalue+bits; done
