FROM ubuntu:19.10

ENV PYTHONUNBUFFERED=1

WORKDIR /opt

RUN set -eux \
    && apt-get update \
    && apt-get install --yes \
        build-essential \
 	    python3-dev \
 	    python3-pip \
    && python3 -m pip install --upgrade pip setuptools wheel \
    && rm -rf /.cache/pip /tmp/pip* \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

COPY . ./

RUN set -eux \
    && python3 -m pip install . \
    && rm -rf /.cache/pip /tmp/pip*

WORKDIR /opt/test/