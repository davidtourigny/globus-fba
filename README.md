# GLOBUS-FBA: Global biochemical reconstruction using sampling
This repository originated as a hard fork of [globus](https://github.com/platyias/globus) authored by German Plata and Jeewoen Sin.

The current version of this project is now available as a *Python* software package based on a combination of pure *Python* modules and a *C++* extension *Python* module.

## Building and installation

*GLOBUS-FBA* runs on *Linux* and *OSX*, and can be installed directly from source from the root of this repository by running the command:
```
pip install .
```

Alternatively, a [Dockerfile](./Dockerfile) is provided for building a [Docker](https://docs.docker.com/) image to run the software from an interactive container. The image can be built in one step by issuing the command:
```
make build
```
from the root of this repository. It can then be started using
```
make run
```

## Running the test example

*GLOBUS-FBA* can be tested using the example genome [Test.fas](./test/Test.fas) supplied with this repository. The script [test.py](./test/test.py) will run the pipeline provided the path to a database directory containing the required data files is provided as an argument. For example, using the command
```
python test.py <path to database>
```
The relevant BLAST files can be generated beforehand using the script [run_blasts](./run_blasts.sh) independently of *GLOBUS-FBA* and results can be processed after the pipeline has completed using the script [run_sorted](./run_sorted.sh). Please be aware that a reference database is required for running *GLOBUS-FBA*. This database can be made available to users upon request.

## Maintainers of *GLOBUS-FBA*

This repository is currently maintained by

* David S. Tourigny
* Konstantine M. Tchourine

If you use *GLOBUS-FBA*, please cite the paper [Global probabilistic annotation of metabolic networks enables enzyme discovery](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3696893/) by Germán Plata, Tobias Fuhrer, Tzu-Lin Hsiao, Uwe Sauer, and Dennis Vitkup.
