## Test genome

*GLOBUS-FBA* can be tested using the example genome [Test.fas](./Test.fas) supplied in this directory. The script [test.py](./test.py) will run the pipeline provided the path to a database directory containing the required data files is provided as an argument. For example, using the command
```
python test.py <path to database>
```
The relevant BLAST files can be generated beforehand using the script [run_blasts](../run_blasts.sh) independently of *GLOBUS-FBA*. If run on a cluster, the script [job](./job.sh) can serve as an example job for submission from this directory where it is assumed that *GLOBUS-FBA* has been installed in a [conda](https://docs.conda.io/en/latest/) environment and reference database copied to the location specified on lines 6 and 9.

Results can be processed after the pipeline has completed using the script [run_sorted](../run_sorted.sh). Please be aware that a reference database is required for running *GLOBUS-FBA*. This database can be made available to users upon request.
