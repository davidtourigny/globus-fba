import sys
from globus_fba import genome

gen = genome.Genome("Test.fas.input")
gen.database = sys.argv[1]

gen.run_ublast()

gen.run_homology()

gen.run_blastprofile()

gen.run_phylogeny()

gen.run_context()

gen.run_bbhits()

gen.run_orthology()

gen.run_clustering()

gen.run_cgibbs()
