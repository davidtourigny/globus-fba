#!/bin/bash
#$ -l mem=16G,time=6:30:00 
#$ -S /bin/bash
#$ -cwd
#$ -j y
./run_blasts.sh Test.fas ../../globus_database
module load conda
source activate globus
python3 test.py ../../globus_database/
conda deactivate



